# XFilter

This repository contains related scripts, documents, and README files
for the XFilter project.

## Source Code

The source code of XFilter is located in
**[Linux](https://gitlab.com/xfilter/linux)**, a separate repository. Its
structure is going to be organized by following the official kernel tree. To
reduce the size, currently, the repository only has the "security" directory,
where all XFilter functions are implemented.

The code should always be pushed and tagged in `master`. The `upstream` branch,
which contains the official kernel release, is created for the source code
comparing and upgrading.

## Scripts 

The code used in the evaluation (both functional and performance), the dracut
initialization, and the package building can be found under **/scripts**.
