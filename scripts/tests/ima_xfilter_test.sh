#!/bin/bash

# This script checks the runtime measurement table and xlist config file. It
# ensures the files that have been evaluated have been covered in xlist which
# is installed from RPM package.

app='apache2'
app_process='httpd-prefork'
divider_pattern='\/etc\/sysconfig\/ima-policy'
xlist="/etc/ima_xfilter/$app/xlist"
runtime_list='/sys/kernel/security/integrity/ima/ascii_runtime_measurements'

tmptestdir='/tmp/imatest'

ima_xlist='/etc/sysconfig/ima-xlist'
tmp_extracted_xlist="$tmptestdir$xlist"
tmp_sysconfig_xlist="$tmptestdir/ima-xlist"
tmp_runtime_list="$tmptestdir/ascii_runtime_files"
tmp_add_list="$tmptestdir/additional_files"

test_result=0

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

trap 'clean' SIGINT
clean(){
  [[ $(pgrep $app_process) ]] && systemctl stop $app
  [ -d $tmptestdir ] && rm -rf $tmptestdir
}

usage(){
  echo "Usage: $(basename $0) [xlist|xlabel] rpm_package"
}

appconf_prep(){
  cp $ima_xlist $tmp_sysconfig_xlist 
}

app_launch(){
  echo -n "Lanunching $app ... "
  $(systemctl start $app)
  [[ "$?" != 0 ]] && echo "Failed to launch application" && exit
  echo "done!"
}

add_launch(){
  while IFS= read -r line
  do
    [[ $xfilter_method == "xlabel" ]] \
        && setfattr -x security.xlabel $list &>/dev/null # Ensure no xattr
    eval "$line &>/dev/null"
  done <  $tmp_add_list
}

xlist_extract(){
  echo -n "Extracting xlist from the package ... "
  rpm2cpio $rpmfile |cpio -id -D $tmptestdir "*xlist" 2>/dev/null
  echo "done!"
}

tmpdir_create(){
  [ -d $tmptestdir ] && rm -rf $tmptestdir
  mkdir -p $tmptestdir
  echo "Test directory $tmptestdir created."
}

runtime_filter(){
  sed -n "/$divider_pattern/,\$p" $runtime_list \
  |grep -v -E '^10 0{10,}' \
  |grep -v "$divider_pattern" \
  |awk -F' ' '{print $5}' > $tmp_runtime_list
}

add_list_prep(){
  cat << EOF >  $tmp_add_list
/usr/bin/zgrep
/usr/bin/znew
EOF
}

add_list_check(){
  # Test the files in additional list.
  # They should not appear in the measurement list.
  while IFS= read -r line
  do
    grep $line $tmp_runtime_list 2>/dev/null
    if [[ $? != 0 ]]; then
      echo "$line is not in the measurement list"
    else
      echo "${red}Should not appeared in the measurement list:${reset} $line"
      test_result=1
    fi
  done < $tmp_add_list
}

test_xlist(){
  #
  echo -e "\nTesting extracted list against $ima_xlist ...\n"
  while IFS= read -r line
  do
      if [ ! -z $(grep $line$ $tmp_sysconfig_xlist) ];then
         echo "Found: $line"
      else
         echo "${red}Not found: $line${reset}"
	 test_result=1
      fi
  done < $tmp_extracted_xlist
 
  # 
  runtime_filter
  echo -e "\nTesting runtime list against $ima_xlist ...\n"
  while IFS= read -r line
  do
      if [ ! -z $(grep $line$ $tmp_sysconfig_xlist) ];then
         echo "Found: $line"
      else
         echo "${red}Not found: $line${reset}"
	 test_result=1
      fi
  done < $tmp_runtime_list

  echo -e "\nChecking the files not in xlist ...\n"
  add_list_check
}

test_xlabel(){
  echo -e "\nExaming the 'security.xlabel' xattr for the files have been measured ...\n"

  runtime_filter

  while IFS= read -r line
  do
    gresult=$(getfattr --absolute-names --only-values -d -m security.xlabel $line)
    if [ ! -z $gresult ]; then
         echo "Found xattr in $line"
      else
         echo "${red}xattr not found:${reset} $line"
	 test_result=1
    fi
  done < $tmp_runtime_list

  echo -e "\nChecking the files not labeled ...\n"
  add_list_check
}

# main
# =====
#

[[ "$#" != 2 ]] || \
([ "$1" != "xlist" ] && [ "$1" != "xlabel" ]) && \
  usage && \
  exit

xfilter_method=$1
rpmfile=$2

[ ! -f $rpmfile ] &&  \
  echo "$rpmfile is not exist!" && \
  exit

[[ "$(id -u)" != 0 ]] && \
  echo "Need root permission to read runtime list" && \
  exit

clean

tmpdir_create
xlist_extract
add_list_prep
appconf_prep

echo -e "\nRuntime measurement list: $runtime_list"
echo "Extracted xlist file: $tmp_extracted_xlist"
echo -e "\nStart testing ..."

app_launch
add_launch

case "$xfilter_method" in
  'xlist')
    test_xlist
    ;;
  'xlabel')
    test_xlabel
    ;;
  *)
    exit
    ;;
esac

if [[ $test_result == 0 ]]; then
  echo -e "\nTest Result: ${green}**Success**${reset}"
else
  echo -e "\nTest Result: ${red}**Failed**${reset}"
fi

clean
