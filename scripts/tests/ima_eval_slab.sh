#!/bin/bash

# This script evaluates the memory slabs allocated by kernel
# Running on the host!
# usage: ./ima_eval_slab.sh n

tmptracedata='/tmp/trace.dat'
tmptrfile='/dev/shm/trimmed'

ima_policy_remote='/sys/kernel/security/integrity/ima/policy'

TESTCOUNT=40
GUEST='guestsshname'
[ -z $TRIMMED ] && TRIMMED=2
SCALE=1
[ -z $SLEEP ] && SLEEP=30

[ -z $NEED_MEAN_CALC ] && NEED_MEAN_CALC=1
[ -z $NEED_MEANS_TRIMMED_CALC ] && NEED_MEANS_TRIMMED_CALC=0
[ -z $MEAN_CALC_ONLY ] && MEAN_CALC_ONLY=0

trap 'clean' SIGINT
clean(){
  [ -f $tmptrfile ]    && rm -f $tmptrfile
  [ -f $tmptracedata ] && rm $tmptracedata
  EXITC=1
  unset TESTCOUNT GUEST NEED_MEAN_CALC NEED_MEANS_TRIMMED_CALC MEAN_CALC_ONLY
}

usage(){
  echo "Usage: $(basename $0) n"
}

test_ima_status_r() {
  [[ $# != 1 ]] && echo "$0 arguments err" && exit

  [[ $(ssh $GUEST grep XF_XLIST $ima_policy_remote) ]] && {
    xlist_c=$(ssh $GUEST cat /sys/kernel/security/integrity/ima/xlist | wc -l)
    ima_status="$1.ima_xlist.$xlist_c"
    bootn='1'
    return
  }

  [[ $(ssh $GUEST grep XF_XLABEL $ima_policy_remote) ]] && {
    ima_status="$1.ima_xlabel"
    bootn='1'
    return
  }

  [[ $(ssh $GUEST grep 'measure.*func.*mask' $ima_policy_remote) ]] && {
    ima_status="$1.ima_tcb"
    bootn='1'
    return
  }

  ima_status="$1.ima_disabled"
  bootn='0'

  return
}

gen_report() {

  solve_log_exist $report_file

  # Add header
  echo $ima_status | tee -a $report_file
  
  last_c=$(( $TESTCOUNT - 1 ))
  for ((c=0; c < $TESTCOUNT; c++)); do 
    [ $EXITC ] && break
  
    echo -n "$c "
    data=$(ssh $GUEST grep ^Slab /proc/meminfo | awk '{print $2}')
    echo $data | tee -a $report_file
  
    ssh $GUEST grub2-reboot $bootn
    ssh $GUEST reboot
  
    [ $c != "$last_c" ] && sleep $SLEEP
  
  done
}

## main

source "$(dirname "$0")/ima_eval_include.sh"

[[ "$#" > 1 ]] && usage && exit
report_dir='./'

test_ima_status_r 'slab'

[[ "$#" == 0 ]] &&  report_file="$report_dir/$ima_status.log"
[[ "$#" == 1 ]] &&  report_file="$report_dir/$ima_status.$1.log"

[[ $MEAN_CALC_ONLY != 1 ]] && \
  gen_report && \
  draw_hl $report_file

[[ $NEED_MEAN_CALC == 1 ]]          && means         $report_file 1
[[ $NEED_MEANS_TRIMMED_CALC == 1 ]] && means_trimmed $report_file 1

clean
