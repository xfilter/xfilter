#!/bin/bash

# This script evaluates the performance of xfilter extension
# by using `time` command to `tar`

tmptestdir='/tmp/testdir'
tmpdir='/dev/shm'
tmptrfile="$tmpdir/trimmed"
tmptarball='/dev/shm/t.tar.gz'

TESTCOUNT=100
[ -z $TRIMMED ] && TRIMMED=5
CPU_NUM=2
SCALE=3
[ -z $SLEEP ] && SLEEP=10

[ -z $NEED_MEAN_CALC ] && NEED_MEAN_CALC=1
[ -z $NEED_MEANS_TRIMMED_CALC ] && NEED_MEANS_TRIMMED_CALC=1
[ -z $MEAN_CALC_ONLY ] && MEAN_CALC_ONLY=0

trap 'clean' SIGINT
clean(){
  swapon -a
  [ -d $tmptestdir ] && rm -rf $tmptestdir
  [ -f $tmptrfile ] && rm -f $tmptrfile
  [ -f $tmptarball ] && rm -f $tmptarball
  rm -f "$tmpdir/$ima_status*.tmp"
  unset TIMEFORMAT TRIMMED NEED_MEAN_CALC NEED_MEANS_TRIMMED_CALC TRIMMED MEAN_CALC_ONLY
  EXITC=1
}

usage(){
  echo "Usage: $(basename $0) n"
}


gen_report_time() {

  solve_log_exist $report_file

  report_tmp=$tmpdir/$ima_status.$(echo $1 | awk '{print $1}').tmp
  [ -f $report_tmp ] &&  rm -f $report_tmp

  #echo "time: $1" | sed s/\$.*$//g | tee -a $report_tmp
  cmd_name=$(echo $1 | awk '{print $1}')
  echo "$ima_status.$cmd_name" | tee -a $report_tmp

  for ((c=0; c < $TESTCOUNT; c++)); do 
    [ $EXITC ] && break
    [ ! -z $SLEEP ] && sleep $SLEEP
    cache_clean

    echo -n "$c "

    (time (for i in {1..128}; do echo 3 > /proc/sys/vm/drop_caches; \
                                 eval $1 &>/dev/null; done)) |& tee -a $report_tmp

    test $? -gt 128 && break
  done

  # [ -f $report_file ] && echo -e '\n----------\n' >> $report_file
  cat $report_tmp >> $report_file && rm -f $report_tmp
}

#######
# main
#######

source "$(dirname "$0")/ima_eval_include.sh"

[[ "$#" > 1 ]] && usage && exit
report_dir='./'

test_cpu $CPU_NUM
test_dir $report_dir

test_ima_status 'time'
testdir_prep $tmptestdir

[[ "$#" == 0 ]] &&  report_file="$report_dir/$ima_status.log"
[[ "$#" == 1 ]] &&  report_file="$report_dir/$ima_status.$1.log"

# Switch off the swap
swapoff -a

#[[ $MEAN_CALC_ONLY != 1 ]] && gen_report_time 'ls -la $tmptestdir' && draw_hl $report_file
[[ $MEAN_CALC_ONLY != 1 ]] && \
  gen_report_time 'tar czf $tmptarball $tmptestdir 2>/dev/null' && \
  draw_hl $report_file

[[ $NEED_MEAN_CALC == 1 ]]          && means         $report_file 1
[[ $NEED_MEANS_TRIMMED_CALC == 1 ]] && means_trimmed $report_file 1

clean
