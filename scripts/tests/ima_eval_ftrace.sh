#!/bin/bash

# This script evaluates the performance of xfilter extension
# by using ftrace (trace-cmd) to a customized command `hello`

test_comm='hello'
tmptracedata='/tmp/trace.dat'
tmptrfile='/dev/shm/trimmed'

CPU_NUM=2
TESTCOUNT=100
[ -z $TRIMMED ] && TRIMMED=5
SCALE=3
[ -z $SLEEP ] && SLEEP=10

[ -z $NEED_MEAN_CALC ] && NEED_MEAN_CALC=1
[ -z $NEED_MEANS_TRIMMED_CALC ] && NEED_MEANS_TRIMMED_CALC=1
[ -z $MEAN_CALC_ONLY ] && MEAN_CALC_ONLY=0

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

trap 'clean' SIGINT
clean(){
  swapon -a
  [ -f $tmptrfile ]    && rm -f $tmptrfile
  EXITC=1
  unset TIMEFORMAT TRIMMED NEED_MEAN_CALC NEED_MEANS_TRIMMED_CALC MEAN_CALC_ONLY
  [ -f $tmptracedata ] && rm $tmptracedata
}

usage(){
  echo "Usage: $(basename $0) n"
}

gen_report() {

  solve_log_exist $report_file

  for s in "${trace_func[@]}"; do
    log_header+=$(echo -e -n "$ima_status.$s,")
    func_opts=$(echo "$func_opts -l $s")  # concatenate -l options
  done
  echo_record $log_header | tee ${report_file}
  
  for ((c=0; c < $TESTCOUNT; c++)); do 
    [ $EXITC ] && break
    [ ! -z $SLEEP ] && sleep $SLEEP
    cache_clean

    echo -n "$c "

    trace-cmd record \
        -o $tmptracedata \
        -p function_graph \
        $func_opts \
        $test_comm 2>/dev/null

    test $? -gt 128 && break
  
    for i in "${trace_func[@]}"; do
      duration[$i]=$(trace-cmd report -i $tmptracedata --comm=$test_comm -O fgraph:tailprint=yes | \
                         sed 's/\+ //g' | \
                         sed 's/\! //g' | \
                         sed 's/\# /  /g' | \
         	             grep "[0-9] us.*$i" | \
                  	     head -n 1 | \
                  	     awk '{print $5}')
  
      [ -z ${duration[$i]} ] && {
        echo "Not found: the duration of $i"
        [ ! -z $SLEEP ] && sleep $SLEEP
        ((c-=1))
        continue 2
      }
  
      # "+" Meaning: duration exceeded 10 usecs
      #  See lib/trace-cmd/trace-ftrace.c
      #
      # [ $(echo ${duration[$i]} | grep '^\+') ] && {
      #    duration[$i]=${duration[$i]//+}
      #    illegal_fmt=1
      # }
      
    done
  
    # "+" Meaning: duration exceeded 10 usecs
    #  See lib/trace-cmd/trace-ftrace.c
    #
    # [ ! -z $illegal_fmt ] && {
    #     echo -n " +"
    #     unset illegal_fmt
    # }
    
    unset log_entry
    for t in "${trace_func[@]}"; do
      log_entry+=$(echo -n -e "${duration[$t]},")
    done
    echo_record -n $log_entry |tee -a ${report_file} 
  
    echo |tee -a ${report_file}
  done

} #gen_report()


#######
# main
#######

source "$(dirname "$0")/ima_eval_include.sh"

[[ "$#" > 1 ]] && usage && exit
report_dir='./'

test_cpu $CPU_NUM
test_dir $report_dir

test_comm "$test_comm"
test_ima_status 'ftrace'

[[ "$#" == 0 ]] &&  report_file="$report_dir/$ima_status.log"
[[ "$#" == 1 ]] &&  report_file="$report_dir/$ima_status.$1.log"

# Switch off the swap
swapoff -a

declare trace_func=( 'ima_file_check' 'ima_bprm_check' 'ima_file_mmap' )
declare -A duration=()

[[ $MEAN_CALC_ONLY != 1 ]] && \
  gen_report && \
  draw_hl $report_file

[[ $NEED_MEAN_CALC == 1 ]]          && means         $report_file 3
[[ $NEED_MEANS_TRIMMED_CALC == 1 ]] && means_trimmed $report_file 3

clean
