#!/bin/sh

if [ $# != 1 ]; then
  echo -e "Error: option number\nExit"
  exit 1
fi

flist=$1
xlist_conf='/etc/sysconfig/ima-xlist'

[ ! -f $flist ] && echo "Can not find file: $flist" && exit 1

echo -n "Updating $xlist_conf ... "
cp $xlist_conf $xlist_conf.rpmsave
echo -e "$(cat $flist)\n$(cat $xlist_conf.rpmsave)" | sort -r | uniq > $xlist_conf

echo "done"
