#!/bin/sh

if [ $# != 1 ]; then
  echo -e "Error: option number\nExit"
  exit 1
fi

flist=$1
xlabel='security.xlabel'

[ ! -f $flist ] && echo "Can not find file: $flist" && exit 1

echo -n "Labeling files ... "

while IFS= read -r line; do
  setfattr -n $xlabel -v 1 $(echo "$line" | sed "s/^file=//g")
done < "$flist"

echo "done"
